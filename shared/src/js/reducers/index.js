import { handleActions } from "redux-actions"
import defaultState from "../stores/defaultState.js";
import { ACTIONS } from "../actions"

const reducers = {};

reducers[ACTIONS.INIT_APP] = (state, action) => {
    let issues = action.payload;

    return Object.assign({}, state, {
        issues: issues
    });
};

export default handleActions(reducers, defaultState);