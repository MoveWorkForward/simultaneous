import React from "react";
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import IssueGridContainer from "./containers/IssueGridContainer.js";
import configureStore from 'stores/configureStore.js'
import { startApp } from "./actions";
import defaultState from "./stores/defaultState.js";

import "../less/index.less"

export default function() {
    var store = configureStore(Object.assign({}, defaultState));
    store.dispatch(startApp("SSP"));

    var app = <Provider store={ store }><IssueGridContainer /></Provider>;
    render(app, document.getElementById("issue-grid"));
}