import React, { PropTypes, Component } from "react";
import Header from "./header.js";
import Issue from "./Issue";

    class IssueGrid extends Component {

    render() {
        const issues = this._getIssues();
        return (

            <div>
                <Header />
                <div className="aui-page-panel main-panel">
                    <div className="aui-page-panel-inner">
                        <section className="aui-page-panel-item">
                            <div className="aui-group">
                                <div className="aui-item">
                                    {issues}
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        )
    }

    _getIssues() {
        return <ol>{this.props.issues.map(issue => <Issue key={issue.key} issue={issue} baseUrl={this.props.baseUrl} />)}</ol>;
    }
}

IssueGrid.defaultProps = {
    baseUrl: "/"
};

IssueGrid.propTypes = {
    issues: PropTypes.array.isRequired,
    baseUrl: PropTypes.string.isRequired
};

export default IssueGrid;