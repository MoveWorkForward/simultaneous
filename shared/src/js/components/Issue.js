import React, { PropTypes, Component } from "react";

class Issue extends Component {

    render() {
        return (
            <li>
                <a href={`${this.props.baseUrl}/browse/${this.props.issue.key}`}>{this.props.issue.key}</a>
                <p>{this.props.issue.fields.summary}</p>
            </li>
        )
    }
}

Issue.propTypes = {
    baseUrl: PropTypes.string.isRequired,
    issue: PropTypes.object.isRequired
};

export default Issue;