import fetch from 'isomorphic-fetch'

export default function(url, options) {
    url = AJS.contextPath() + url;
    return fetch(url, options);
}